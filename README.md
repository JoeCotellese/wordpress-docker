# My Wordpress Dev Environment

This is my setup for doing Wordpress development work with Docker.

The setup works as follows:

Customize your Wordpress image in the Dockerfile directory

Customize your environment setup in docker-compose

Create the following directories:
--data
- wordpress
- plugins

* `data` contains the Wordpress database image
* `wordpress` contains the Wordpress folder
* `plugins` contains the Wordpress plugins

## How to use

Clone this repo for each Wordpress project.

Modify the Dockerfile for your particular setup

Profit

## Dockerfile

The Dockerfile starts from the base Wordpress Docker container.
It customizes that container in the ADD and RUN lines. In this case, it's just installing some PHP extensions.

## Wordpress Plugins
Most of the time I'm using this environment to clone a production WP environment when making changes. 

For that, I use the excellent plugins [All In One WP Migration](https://servmask.com) & AIOWP Migration Unlimited (a paid add on)

This plugin is installed in production and on the dev environments. Production is exported -> imported into dev
