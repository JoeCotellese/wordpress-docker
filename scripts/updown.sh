#!/usr/bin/env bash

# Check if SITE_NAME is provided
if [ -z "$1" ]; then
    echo "Error: No site name provided."
    echo "Usage: $0 <site_name> [start|stop]"
    exit 1
fi

SITE_NAME=$1
ACTION=${2:-start}

export SITE_NAME = $SITE_NAME
case "$ACTION" in
    start)
        echo "Starting site: $SITE_NAME"
        docker-compose -p $SITE_NAME up -d
        ;;
    stop)
        echo "Stopping site: $SITE_NAME"
        docker-compose -p "$SITE_NAME" down
        ;;
    *)
        echo "Unknown action: $ACTION"
        echo "Usage: $0 <site_name> [start|stop]"
        exit 1
        ;;
esac